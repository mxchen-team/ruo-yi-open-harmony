import CommonConstants from '../../common/constants/CommonConstants'
import StyleConstants from '../../common/constants/StyleConstants'
import ItemData from '../../common/entity/ItemData'
import CommonUtils from '../../common/utils/CommonUtils'
import { TitleBarView } from '../../view/TitleBarView'
import { WorkMenuView } from '../../view/WorkMenuView'
import WorkPageModel from '../../viewmodel/WorkPageModel'


@Component
export default struct WorkPage {
  build() {
    Column() {
      TitleBarView({ title: "工作台" })
      //轮播图
      SwiperView()
      //标题
      WorkMidTitle()
      //菜单项
      WorkMenu()
    }
  }
}

@Preview
@Component
struct SwiperView {
  @StorageProp('currentDeviceSize') currentDeviceSize: string = CommonConstants.SM;

  build() {
    Column() {
      Swiper() {
        ForEach(WorkPageModel.getBannerData(), (item: Resource) => {
          Image(item)
            .borderRadius($r('app.float.swiper_radius'))
        }, (item: Resource) => JSON.stringify(item))
      }
      .autoPlay(true)
      .width("95%")
      .itemSpace(this.currentDeviceSize === CommonConstants.SM ?
        0 : StyleConstants.ITEM_SPACE)
      .margin($r('app.float.common_margin'))
      .displayCount(this.currentDeviceSize === CommonConstants.SM ?
      StyleConstants.SWIPER_COUNT_ONE :
        (this.currentDeviceSize === CommonConstants.MD ?
        StyleConstants.SWIPER_COUNT_TWO : StyleConstants.SWIPER_COUNT_THREE))
    }
  }
}

@Preview
@Component
struct WorkMidTitle {
  build() {
    Row() {
      Line()
        .width(4)
        .height(12)
        .startPoint([0, 0])
        .endPoint([0, 12])
        .stroke(Color.Blue)
        .strokeWidth(4)
        .strokeLineCap(LineCapStyle.Butt)
      Text("系统管理")
    }.width("95%").justifyContent(FlexAlign.Start)
  }
}

@Preview
@Component
struct WorkMenu {
  scroller: Scroller = new Scroller()
  @State currentClickIndex: number = -1

  build() {
    Column() {
      Grid(this.scroller) {
        ForEach(WorkPageModel.getMenuData(), (item: ItemData) => {
          GridItem() {
            WorkMenuView({ item: item })
              .onClick(() => {
                CommonUtils.showToast("模块建设中")
              })
          }
          .width("100%")
          .padding({ top: 15, bottom: 15, left: 0, right: 0 })
        })
      }
      .columnsTemplate('1fr 1fr 1fr 1fr')
    }.width('95%').height("100%").margin({ top: 5 })
  }
}
